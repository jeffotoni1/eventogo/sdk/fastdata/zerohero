package cacheram

import "testing"

func BenchmarkFreeCacheSet(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		freeCacheSet(key, val, 3600)
	}
	b.StopTimer()
}

func BenchmarkFreeCacheGet(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_ = freeCacheGet(key)
	}
	b.StopTimer()
}

func BenchmarkGoCacheSet(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		goCacheSet(key2, val2)
	}
	b.StopTimer()
}

func BenchmarkGoCacheGet(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_ = goCacheGet(key2)
	}
	b.StopTimer()
}

func BenchmarkGoCacheStructSet(b *testing.B) {
	b.ResetTimer()
	//var hero *Hero
	for n := 0; n < b.N; n++ {
		goCacheStructSet(key3, hero)
	}
	b.StopTimer()
}

func BenchmarkGoCacheStructGet(b *testing.B) {
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_ = goCacheStructGet(key3)
	}
	b.StopTimer()
}
