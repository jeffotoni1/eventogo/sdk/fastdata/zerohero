package cacheram

import (
	"fmt"
	"runtime/debug"
	"time"

	"github.com/coocood/freecache"
	"github.com/patrickmn/go-cache"
)

var (
	gcache = cache.New(5*time.Minute, 10*time.Minute)

	fcacheSize = 100 * 1024 * 1024
	fcache     = freecache.NewCache(fcacheSize)

	key  = []byte("A-BOMB")
	key2 = "HULK"
	key3 = "SUPERMAN"

	val = []byte(`{
        "appearance": {
               "eye-color": "Yellow",
               "gender": "Male",
               "hair-color": "No Hair",
               "height": [
                      "6'8",
                      "203 cm"
               ],
               "race": "Human",
               "weight": [
                      "980 lb",
                      "441 kg"
               ]
        },
        "biography": {
               "aliases": [
                      "Rick Jones"
               ],
               "alignment": "good",
               "alter-egos": "No alter egos found.",
               "first-appearance": "Hulk Vol 2 #2 (April, 2008) (as A-Bomb)",
               "full-name": "Richard Milhouse Jones",
               "place-of-birth": "Scarsdale, Arizona",
               "publisher": "Marvel Comics"
        },
        "connections": {
               "group-affiliation": "Hulk Family; Excelsior (sponsor), Avengers (honorary member); formerly partner of the Hulk, Captain America and Captain Marvel; Teen Brigade; ally of Rom",
               "relatives": "Marlo Chandler-Jones (wife); Polly (aunt); Mrs. Chandler (mother-in-law); Keith Chandler, Ray Chandler, three unidentified others (brothers-in-law); unidentified father (deceased); Jackie Shorr (alleged mother; unconfirmed)"
        },
        "id": "1",
        "image": {
               "url": "https://www.superherodb.com/pictures2/portraits/10/100/10060.jpg"
        },
        "name": "A-Bomb",
        "powerstats": {
               "combat": "64",
               "durability": "80",
               "intelligence": "38",
               "power": "24",
               "speed": "17",
               "strength": "100"
        },
        "response": "success",
        "work": {
               "base": "-",
               "occupation": "Musician, adventurer, author; formerly talk show host"
        }
    }`)

	val2 = string(val)
)

type Hero struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Appearance struct {
		EyeColor  string   `json:"eye-color"`
		Gender    string   `json:"gender"`
		HairColor string   `json:"hair-color"`
		Height    []string `json:"height"`
		Race      string   `json:"race"`
		Weight    []string `json:"weight"`
	} `json:"appearance"`
	Biography struct {
		Aliases         []string `json:"aliases"`
		Alignment       string   `json:"alignment"`
		AlterEgos       string   `json:"alter-egos"`
		FirstAppearance string   `json:"first-appearance"`
		FullName        string   `json:"full-name"`
		PlaceOfBirth    string   `json:"place-of-birth"`
		Publisher       string   `json:"publisher"`
	} `json:"biography"`
	Connections struct {
		GroupAffiliation string `json:"group-affiliation"`
		Relatives        string `json:"relatives"`
	} `json:"connections"`
	Image struct {
		URL string `json:"url"`
	} `json:"image"`
	Powerstats struct {
		Combat       string `json:"combat"`
		Durability   string `json:"durability"`
		Intelligence string `json:"intelligence"`
		Power        string `json:"power"`
		Speed        string `json:"speed"`
		Strength     string `json:"strength"`
	} `json:"powerstats"`
	Response string `json:"response"`
	Work     struct {
		Base       string `json:"base"`
		Occupation string `json:"occupation"`
	} `json:"work"`
}

var hero = &Hero{
	ID:   "1",
	Name: "Superman",
	Appearance: struct {
		EyeColor  string   `json:"eye-color"`
		Gender    string   `json:"gender"`
		HairColor string   `json:"hair-color"`
		Height    []string `json:"height"`
		Race      string   `json:"race"`
		Weight    []string `json:"weight"`
	}{
		EyeColor:  "Blue",
		Gender:    "Male",
		HairColor: "Black",
		Height:    []string{"6'3\"", "191 cm"},
		Race:      "Kryptonian",
		Weight:    []string{"235 lb", "107 kg"},
	},
	Biography: struct {
		Aliases         []string `json:"aliases"`
		Alignment       string   `json:"alignment"`
		AlterEgos       string   `json:"alter-egos"`
		FirstAppearance string   `json:"first-appearance"`
		FullName        string   `json:"full-name"`
		PlaceOfBirth    string   `json:"place-of-birth"`
		Publisher       string   `json:"publisher"`
	}{
		Aliases:         []string{"Clark Kent", "Kal-El"},
		Alignment:       "Good",
		AlterEgos:       "No alter egos found.",
		FirstAppearance: "Action Comics #1 (June, 1938)",
		FullName:        "Clark Joseph Kent",
		PlaceOfBirth:    "Krypton",
		Publisher:       "DC Comics",
	},
	Connections: struct {
		GroupAffiliation string `json:"group-affiliation"`
		Relatives        string `json:"relatives"`
	}{
		GroupAffiliation: "Justice League",
		Relatives:        "Lois Lane (wife), Jonathan Samuel Kent (son), Jor-El (father, deceased), Lara Lor-Van (mother, deceased)",
	},
	Image: struct {
		URL string `json:"url"`
	}{
		URL: "https://www.superherodb.com/pictures2/portraits/10/100/791.jpg",
	},
	Powerstats: struct {
		Combat       string `json:"combat"`
		Durability   string `json:"durability"`
		Intelligence string `json:"intelligence"`
		Power        string `json:"power"`
		Speed        string `json:"speed"`
		Strength     string `json:"strength"`
	}{
		Combat:       "85",
		Durability:   "100",
		Intelligence: "100",
		Power:        "100",
		Speed:        "100",
		Strength:     "100",
	},
	Response: "success",
	Work: struct {
		Base       string `json:"base"`
		Occupation string `json:"occupation"`
	}{
		Base:       "Metropolis",
		Occupation: "Reporter",
	},
}

func main() {
	debug.SetGCPercent(20)

	freeCacheSet(key, val, 3600)
	got := freeCacheGet(key)

	fmt.Printf("%s\n", got)
	fmt.Println("entry count ", fcache.EntryCount())

	goCacheSet(key2, val2)
	got2 := goCacheGet(key2)
	fmt.Printf("%s\n", got2)

	//var hero *Hero
	goCacheStructSet(key3, hero)
	gotI := goCacheStructGet(key3)
	fmt.Printf("Hero: %v\n", gotI.(Hero))
}

func goCacheStructSet(key string, val interface{}) {
	gcache.Set(key, val, cache.DefaultExpiration)
}

func goCacheStructGet(key string) interface{} {
	got, found := gcache.Get(key)
	if !found {
		fmt.Println("Error: ", found)
		return nil
	}
	return got
}

func goCacheSet(key, val string) {
	gcache.Set(key, val, cache.DefaultExpiration)
}

func goCacheGet(key string) string {
	got, found := gcache.Get(key)
	if !found {
		fmt.Println("Error: ", found)
		return ""
	}
	return got.(string)
}

func freeCacheSet(key, val []byte, expire int) {
	fcache.Set(key, val, expire)
}

func freeCacheGet(key []byte) []byte {
	got, err := fcache.Get(key)
	if err != nil {
		fmt.Println(err)
		return []byte(``)
	}
	return got
}

func goCache() {

}
