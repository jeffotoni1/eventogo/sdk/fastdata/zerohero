package memory

import (
	"testing"
)

func TestGetLoadDataByteSuccess(t *testing.T) {
	c := newcache()

	err := c.loadDataByte()
	if err != nil {
		t.Error(err)
	}

	_, ok := c.GetHero("ajax")
	if !ok {
		t.Error("not found")
	}
}

func TestGetLoadDataObjectSuccess(t *testing.T) {
	c := newcache()

	err := c.loadDataObject()
	if err != nil {
		t.Error(err)
	}

	_, ok := c.GetHero("ajax")
	if !ok {
		t.Error("not found")
	}
}

func TestGetLoadDataStringSuccess(t *testing.T) {
	c := newcache()

	err := c.loadDataString()
	if err != nil {
		t.Error(err)
	}

	_, ok := c.GetHero("ajax")
	if !ok {
		t.Error("not found")
	}
}
