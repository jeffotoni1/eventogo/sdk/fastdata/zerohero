package models

import (
	"encoding/json"

	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
)

type Hero struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Appearance struct {
		EyeColor  string   `json:"eye-color"`
		Gender    string   `json:"gender"`
		HairColor string   `json:"hair-color"`
		Height    []string `json:"height"`
		Race      string   `json:"race"`
		Weight    []string `json:"weight"`
	} `json:"appearance"`
	Biography struct {
		Aliases         []string `json:"aliases"`
		Alignment       string   `json:"alignment"`
		AlterEgos       string   `json:"alter-egos"`
		FirstAppearance string   `json:"first-appearance"`
		FullName        string   `json:"full-name"`
		PlaceOfBirth    string   `json:"place-of-birth"`
		Publisher       string   `json:"publisher"`
	} `json:"biography"`
	Connections struct {
		GroupAffiliation string `json:"group-affiliation"`
		Relatives        string `json:"relatives"`
	} `json:"connections"`
	Image struct {
		URL string `json:"url"`
	} `json:"image"`
	Powerstats struct {
		Combat       string `json:"combat"`
		Durability   string `json:"durability"`
		Intelligence string `json:"intelligence"`
		Power        string `json:"power"`
		Speed        string `json:"speed"`
		Strength     string `json:"strength"`
	} `json:"powerstats"`
	Response string `json:"response"`
	Work     struct {
		Base       string `json:"base"`
		Occupation string `json:"occupation"`
	} `json:"work"`
}

func HeroToProtoHero(h *Hero) (p *proto.Hero, err error) {
	b, err := json.Marshal(h)
	if err != nil {
		return
	}

	err = json.Unmarshal(b, &p)
	return
}
