package server

import (
	"context"
	"errors"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/memory"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/models"
)

func convertToProtoHero(hs *models.Hero) *proto.Hero {
	return &proto.Hero{
		Id:   hs.ID,
		Name: hs.Name,
		Appearance: &proto.Appearance{
			EyeColor:  hs.Appearance.EyeColor,
			Gender:    hs.Appearance.Gender,
			HairColor: hs.Appearance.HairColor,
			Height:    hs.Appearance.Height,
			Race:      hs.Appearance.Race,
			Weight:    hs.Appearance.Weight,
		},
		Biography: &proto.Biography{
			Aliases:         hs.Biography.Aliases,
			Alignment:       hs.Biography.Alignment,
			AlterEgos:       hs.Biography.AlterEgos,
			FirstAppearance: hs.Biography.FirstAppearance,
			FullName:        hs.Biography.FullName,
			PlaceOfBirth:    hs.Biography.PlaceOfBirth,
			Publisher:       hs.Biography.Publisher,
		},
		Connections: &proto.Connections{
			GroupAffiliation: hs.Connections.GroupAffiliation,
			Relatives:        hs.Connections.Relatives,
		},
		Image: &proto.Image{
			Url: hs.Image.URL,
		},
		Powerstats: &proto.Powerstats{
			Combat:       hs.Powerstats.Combat,
			Durability:   hs.Powerstats.Durability,
			Intelligence: hs.Powerstats.Intelligence,
			Power:        hs.Powerstats.Power,
			Speed:        hs.Powerstats.Speed,
			Strength:     hs.Powerstats.Strength,
		},
		Response: hs.Response,
		Work: &proto.Work{
			Base:       hs.Work.Base,
			Occupation: hs.Work.Occupation,
		},
	}
}

// func (s *Server) GetID(ctx context.Context, req *proto.StringValue) (*proto.JsonResponse, error) { // string
// func (s *Server) GetID(ctx context.Context, req *proto.StringValue) (*proto.Hero, error) { //struct
func (s *Server) GetID(ctx context.Context, req *proto.StringValue) (*proto.ByteSlice, error) { // byte
	if len(req.Value) == 0 {
		err := errors.New("ID is required")
		return &proto.ByteSlice{}, err
	}

	id := req.Value
	hero, err := connect.GetHeroByID(id)
	if err != nil {
		switch err {
		case memory.ErrNotFound:
			err = fmt.Errorf("hero of ID %s not found", id)
			return &proto.ByteSlice{}, err

		case memory.ErrInvalidAssert:
			err = errors.New("invalid data type stored in memomy")
			return &proto.ByteSlice{}, err

		default:
			return &proto.ByteSlice{}, err
		}
	}

	// return &proto.JsonResponse{Data: hero}, nil // string
	// return models.HeroToProtoHero(hero) // marshal, unmarshal
	// heroObj := convertToProtoHero(hero)
	// return heroObj, nil // convert
	return &proto.ByteSlice{Data: hero}, nil // byte
}

// func (s *Server) GetName(ctx context.Context, req *proto.StringValue) (*proto.ByteSlice, error) { // byte
// func (s *Server) GetName(ctx context.Context, req *proto.StringValue) (*proto.JsonResponse, error) { // string
func (s *Server) GetName(ctx context.Context, req *proto.StringValue) (*proto.ByteSlice, error) { // struct
	if len(req.Value) == 0 {
		err := errors.New("name is required")
		return &proto.ByteSlice{}, err
	}

	name := req.Value
	hero, err := connect.GetHeroByName(name)
	if err != nil {
		switch err {
		case memory.ErrNotFound:
			err = fmt.Errorf("hero of name %s not found", name)
			return &proto.ByteSlice{}, err

		case memory.ErrInvalidAssert:
			err = errors.New("invalid data type stored in memomy")
			return &proto.ByteSlice{}, err

		default:
			return &proto.ByteSlice{}, err
		}
	}
	// fmt.Println("done:", string(hero))
	// return &proto.JsonResponse{Data: hero}, nil // string
	// heroObj := convertToProtoHero(hero)
	// return heroObj, nil // convert
	// return models.HeroToProtoHero(hero) // marshal,unmarshal
	return &proto.ByteSlice{Data: hero}, nil // byte
}

func (s *Server) GetNameList(ctx context.Context, req *empty.Empty) (*proto.ByteSlice, error) { // struct
	hero, err := connect.GetHeroByNameLista()
	if err != nil {
		switch err {
		case memory.ErrNotFound:
			err = fmt.Errorf("hero of name not found")
			return &proto.ByteSlice{}, err

		case memory.ErrInvalidAssert:
			err = errors.New("invalid data type stored in memomy")
			return &proto.ByteSlice{}, err

		default:
			return &proto.ByteSlice{}, err
		}
	}
	// fmt.Println("done:", string(hero))
	// return &proto.JsonResponse{Data: hero}, nil // string
	// heroObj := convertToProtoHero(hero)
	// return heroObj, nil // convert
	// return models.HeroToProtoHero(hero) // marshal,unmarshal
	return &proto.ByteSlice{Data: hero}, nil // byte
}
