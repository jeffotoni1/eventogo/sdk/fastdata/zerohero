package server

import (
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
)

type Server struct {
	proto.UnimplementedHeroServiceServer
}

func New() *Server {
	return &Server{}
}
