package connect

import (
	"context"

	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
)

func GetByID(env Env, protocol, value string) ([]byte, int, error) {
	switch protocol {
	case REST_PROTOCOL:
		return httpGetByID(env, value) // byte, string, struct
	case GRPC_PROTOCOL:
		return grpcGetByID(env, value) // byte, string, struct
	default:
		return []byte(""), 0, ErrInvalidProtocol
	}
}

func httpGetByID(env Env, id string) (data []byte, status int, err error) { // byte
	url := fmts.ConcatStr(env.REST_URL_ID, id)
	return requestHttp(url)
}

func grpcGetByID(env Env, id string) ([]byte, int, error) {
	cli := proto.NewHeroServiceClient(env.GrpcClient)
	req := &proto.StringValue{
		Value: id,
	}
	data, err := cli.GetID(context.Background(), req)
	if err != nil {
		return nil, 0, err
	}
	return data.Data, 0, err
}
