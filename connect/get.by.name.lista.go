package connect

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
)

func GetByNameList(env Env, protocol, value string) ([]byte, int, error) { // byte
	switch protocol {
	case REST_PROTOCOL:
		return httpGetByNameList(env, value)
	case GRPC_PROTOCOL:
		return grpcGetByNameList(env, value)
	default:
		return []byte(""), 0, ErrInvalidProtocol
	}
}

func httpGetByNameList(env Env, name string) (data []byte, status int, err error) { // byte
	url := fmts.ConcatStr(env.REST_URL_NAME_LIST)
	return requestHttp(url)
}

func grpcGetByNameList(env Env, name string) ([]byte, int, error) { // byte
	cli := proto.NewHeroServiceClient(env.GrpcClient)
	// req := &proto.StringValue{
	// 	Value: name,
	// }
	var req = &empty.Empty{}
	data, err := cli.GetNameList(context.Background(), req)
	if err != nil {
		return nil, 0, err
	}
	return data.Data, 0, err
}
