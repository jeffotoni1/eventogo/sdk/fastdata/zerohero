package connect

import (
	"net/http"
	"testing"
)

func TestGetByIdRestSuccess(t *testing.T) {
	_, status, err := GetByID(envTest, REST_PROTOCOL, "13")
	if err != nil {
		t.Error(err)
	}
	if status != http.StatusOK {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByIdRestNotFound(t *testing.T) {
	_, status, err := GetByID(envTest, REST_PROTOCOL, "0")
	if err != nil {
		if status == 404 {
			t.Log("status:", status)
			t.Log(err)
		} else {
			t.Error(err)
		}
	}
	if status != http.StatusNotFound {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByIdGrpcSuccess(t *testing.T) {
	_, status, err := GetByID(envTest, GRPC_PROTOCOL, "13")
	if err != nil {
		t.Error(err)
	}
	if status != 0 {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByIdGrpcNotFound(t *testing.T) {
	_, status, err := GetByID(envTest, GRPC_PROTOCOL, "0")
	if err != nil {
		if status == 0 {
			t.Log("status:", status)
			t.Log(err)
		} else {
			t.Error(err)
		}
	}
	if status != 0 {
		t.Error("unexpected status: ", status)
	}
}
