package connect

import (
	"net/http"
	"testing"
)

func TestGetByNameRestSuccess(t *testing.T) {
	_, status, err := GetByName(envTest, REST_PROTOCOL, "ajax")
	if err != nil {
		t.Error(err)
	}
	if status != http.StatusOK {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByNameRestNotFound(t *testing.T) {
	_, status, err := GetByName(envTest, REST_PROTOCOL, "aaaa")
	if err != nil {
		t.Error(err)
	}
	if status != http.StatusNotFound {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByNameGrpcSuccess(t *testing.T) {
	_, status, err := GetByName(envTest, GRPC_PROTOCOL, "ajax")
	if err != nil {
		t.Error(err)
	}
	if status != http.StatusOK {
		t.Error("unexpected status: ", status)
	}
}

func TestGetByNameGrpcNotFound(t *testing.T) {
	_, status, err := GetByName(envTest, GRPC_PROTOCOL, "aaaa")
	if err != nil {
		t.Error(err)
	}
	if status != http.StatusNotFound {
		t.Error("unexpected status: ", status)
	}
}
