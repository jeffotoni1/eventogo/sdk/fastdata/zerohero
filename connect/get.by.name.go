package connect

import (
	"context"

	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
)

func GetByName(env Env, protocol, value string) ([]byte, int, error) { // byte
	switch protocol {
	case REST_PROTOCOL:
		return httpGetByName(env, value)
	case GRPC_PROTOCOL:
		return grpcGetByName(env, value)
	default:
		return []byte(""), 0, ErrInvalidProtocol
	}
}

func httpGetByName(env Env, name string) (data []byte, status int, err error) { // byte
	url := fmts.ConcatStr(env.REST_URL_NAME, name)
	return requestHttp(url)
}

func grpcGetByName(env Env, name string) ([]byte, int, error) { // byte
	cli := proto.NewHeroServiceClient(env.GrpcClient)
	req := &proto.StringValue{
		Value: name,
	}
	data, err := cli.GetName(context.Background(), req)
	if err != nil {
		return nil, 0, err
	}
	return data.Data, 0, err
}
