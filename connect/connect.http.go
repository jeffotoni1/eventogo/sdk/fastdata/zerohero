package connect

import (
	"context"
	"io"
	"net/http"
	"time"

	e "gitlab.com/jeffotoni1/eventogo/sdk/error"
)

var (
	restClient = &http.Client{Transport: &http.Transport{
		DisableKeepAlives: false,
		MaxIdleConns:      5,
	}}
)

func requestHttp(url string) (data []byte, status int, err error) { // byte
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		status = http.StatusInternalServerError
		return
	}

	req.Header.Set("Accept", "application/json")

	resp, err := restClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	status = resp.StatusCode
	rawData, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = e.NewError(string(rawData))
		return
	}
	data = rawData // byte
	return
}
