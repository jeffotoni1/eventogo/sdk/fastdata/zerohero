package connect

import (
	"log"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewGrpcClient(addr string) *grpcClient {
	return &grpcClient{
		addr: addr,
		once: new(sync.Once),
	}
}

func (c *grpcClient) ConnectClient() *grpc.ClientConn {
	c.once.Do(func() {
		cli, err := grpc.Dial(
			c.addr,
			grpc.WithTransportCredentials(insecure.NewCredentials()), // grpc.WithBlock()
		)
		if err != nil {
			log.Println("Error when trying to connect to Grpc:", err)
			return
		}
		c.client = cli
	})
	return c.client
}
