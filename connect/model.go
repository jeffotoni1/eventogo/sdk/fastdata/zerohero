package connect

import (
	"errors"
	"sync"

	"google.golang.org/grpc"
)

type GetFunc func(Env, string, string) ([]byte, int, error) // byte
// type GetFunc func(string, string) (string, int, error) // string
// type GetFuncGrpc func(string, string) ([]byte, int, error) // struct
// type GetFuncHttp func(string, string) ([]byte, int, error) // byte

type grpcClient struct {
	addr   string
	client *grpc.ClientConn
	once   *sync.Once
}

type Env struct {
	GFASTDATA_REST_ADDR string
	GFASTDATA_GRPC_ADDR string
	REST_URL_NAME       string
	REST_URL_NAME_LIST  string
	REST_URL_ID         string
	GrpcClient          *grpc.ClientConn
}

var (
	REST_PROTOCOL = "http"
	GRPC_PROTOCOL = "grpc"
	// REST_URL_NAME      string
	// REST_URL_ID        string
	ErrInvalidProtocol = errors.New("invalid protocol")
)

// env para testes
var (
	GFASTDATA_GRPC_ADDR = "0.0.0.0:50051"
	envTest             = Env{
		GFASTDATA_REST_ADDR: "0.0.0.0:3000",
		GFASTDATA_GRPC_ADDR: GFASTDATA_GRPC_ADDR,
		REST_URL_NAME:       "http://127.0.0.1:3000/v1/hero/name/",
		REST_URL_ID:         "http://127.0.0.1:3000/v1/hero/id/",
		REST_URL_NAME_LIST:  "http://127.0.0.1:3000/v1/hero/list",
		GrpcClient:          NewGrpcClient(GFASTDATA_GRPC_ADDR).ConnectClient(),
	}
)
