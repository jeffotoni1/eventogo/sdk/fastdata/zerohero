package connect

import (
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/memory"
)

// func GetHeroByID(id string) (string, error) { // string
// func GetHeroByID(id string) (*models.Hero, error) { // struct
func GetHeroByID(id string) ([]byte, error) { // byte
	rawHero, ok := memory.Cache.GetHero(id)
	if !ok {
		return nil, memory.ErrNotFound
	}

	// hero, ok := rawHero.(string) // string
	// hero, ok := rawHero.(*models.Hero) // struct
	hero, ok := rawHero.([]byte) // byte
	if !ok {
		return nil, memory.ErrInvalidAssert
	}
	return hero, nil
}

// func GetHeroByName(name string) (string, error) { // string
// func GetHeroByName(name string) (*models.Hero, error) { // struct
func GetHeroByName(name string) ([]byte, error) { // byte
	name = memory.FormatKey(name)
	rawID, ok := memory.Cache.GetHero(name)
	if !ok {
		return nil, memory.ErrNotFound
	}

	id, ok := rawID.(string)
	if !ok {
		return nil, memory.ErrInvalidAssert
	}

	rawHero, ok := memory.Cache.GetHero(id)
	if !ok {
		return nil, memory.ErrNotFound
	}

	// hero, ok := rawHero.(string) // string
	// hero, ok := rawHero.(*models.Hero) // struct
	hero, ok := rawHero.([]byte) // byte
	if !ok {
		return nil, memory.ErrInvalidAssert
	}
	return hero, nil
}

func GetHeroByNameLista() ([]byte, error) { // byte
	rawID, ok := memory.Cache.GetHero("heros")
	if !ok {
		return nil, memory.ErrNotFound
	}

	heros, ok := rawID.([]byte)
	if !ok {
		return nil, memory.ErrInvalidAssert
	}

	return heros, nil
}
